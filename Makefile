
GST_PACKAGE = gst-package
GST_CONVERT = gst-convert

CONVERT_RULES = -r'Osmo.LogManager->LogManager' \
		-r'(Transcript nl)->(Transcript cr)' \
		-r'(thisContext parentContext)->(thisContext sender)' \
		-r'(``@object backtraceOn: ``@args1)->(``@object printOn: ``@args1)' \
		-r'ProcessVariable->GSTProcessVariable'
	

all:
	$(GST_PACKAGE) --test package.xml

convert:
	$(GST_CONVERT) $(CONVERT_RULES) -F squeak -f gst \
		-o fileout.st compat_for_pharo.st LogManager.st
