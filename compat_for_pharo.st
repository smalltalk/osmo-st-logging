Object subclass: GSTProcessVariableHolder [
    | val |
    <category: 'OsmoLogging-Pharo'>

    value: aValue [
        <category: 'set'>
        val := val.
    ]

    value [
        <category: 'get'>
        ^ val
    ]
]

ProcessLocalVariable subclass: GSTProcessVariable [
    | dict |
    <category: 'OsmoLogging-Pharo'>

    GSTProcessVariable class >> key: aKey [
        <category: 'creation'>
        ^ self soleInstance key: aKey.
    ]

    dict [
        <category: 'creation'>
        ^ dict ifNil: [dict := Dictionary new].
    ]

    key: aKey [
        <category: 'key'>
        ^ self dict at: aKey ifAbsent:
                [self dict at: aKey put: GSTProcessVariableHolder new].
    ]
]
